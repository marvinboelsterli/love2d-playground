-- Load some default values for our rectangle.
function love.load()
    love.window.setMode(800, 400, {resizable=true, minwidth=800, minheight=400, vsync=0})

    drawType = 'increase'
    width, height = love.graphics.getDimensions()
    x, y, w, h = 20, 20, 60, 20
end

function love.resize(w, h)
    width = w
    height = h
    print(("Window resized to width: %d and height: %d."):format(w, h))
end

-- Increase the size of the rectangle every frame.
function love.update(dt)
    if 'increase' == drawType then
        w = w + 1
        h = h + 1

        if w == width or h == height then
            drawType = 'decrease'
        end
    else
        w = w - 1
        h = h - 1

        if 0 == w then
            drawType = 'increase'
        end
    end
end

-- Draw a coloured rectangle.
function love.draw()
    -- In versions prior to 11.0, color component values are (0, 102, 102)
    love.graphics.setColor(0, 0.4, 0.4)
    love.graphics.rectangle("fill", x, y, w, h)
end